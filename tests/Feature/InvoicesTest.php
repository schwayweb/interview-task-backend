<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Helpers\InvoiceHelper;
use App\Modules\Invoices\Infrastructure\Database\Seeders\CompanySeeder;
use App\Modules\Invoices\Infrastructure\Database\Seeders\ProductSeeder;
use App\Modules\Invoices\Infrastructure\Database\Seeders\InvoiceSeeder;
use App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder;
use LogicException;

class InvoicesTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(CompanySeeder::class);
        $this->seed(ProductSeeder::class);
        $this->seed(InvoiceSeeder::class);
        $this->seed(DatabaseSeeder::class);
    }

    /**
     * Test if invoice data can be displayed.
     */
    public function testCanShowInvoiceData(): void
    {
        $invoiceHelper = new InvoiceHelper();
        $invoice = $invoiceHelper->getInvoice();

        $response = $this->getJson('/api/invoices/' . $invoice->id);

        $response->assertStatus(200)
            ->assertJsonPath('id', $invoice->id)
            ->assertJsonPath('invoice_number', $invoice->invoice_number);
    }

    /**
     * Test if invoice can be approved.
     */
    public function testCanApproveInvoice(): void
    {
        $invoiceHelper = new InvoiceHelper();
        $invoice = $invoiceHelper->getInvoice();

        $response = $this->approveInvoice($invoice->id);

        if ($response->status() === 200) {
            $response->assertStatus(200)
                ->assertJson(['message' => 'Invoice approved']);
        } else {
            $response->assertJson(['message' => 'approval status is already assigned']);
        }
    }

    /**
     * Test if invoice can be rejected.
     */
    public function testCanRejectInvoice(): void
    {
        $invoiceHelper = new InvoiceHelper();
        $invoice = $invoiceHelper->getInvoice();

        $response = $this->rejectInvoice($invoice->id);

        if ($response->status() === 200) {
            $response->assertStatus(200)
                ->assertJson(['message' => 'Invoice rejected']);
        } else {
            $response->assertJson(['message' => 'approval status is already assigned']);
        }
    }

    private function approveInvoice($invoiceId){
        try{
            $response = $this->postJson('/api/invoices/'. $invoiceId . '/approve');
        } catch (LogicException $e) {
            return response()->json(['message' => $e->getMessage()]);
        }

        return $response;
    }

    private function rejectInvoice($invoiceId){
        try{
            $response = $this->postJson('/api/invoices/'. $invoiceId . '/reject');
        } catch (LogicException $e) {
            return response()->json(['message' => $e->getMessage()]);
        }

        return $response;
    }
}