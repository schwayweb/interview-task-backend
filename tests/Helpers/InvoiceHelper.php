<?php

namespace Tests\Helpers;

use Illuminate\Support\Facades\App;
use App\Modules\Invoices\Domain\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\CompanyModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\ProductModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\InvoiceModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\InvoiceProductLineModel;
use App\Modules\Invoices\Infrastructure\Persistence\EloquentInvoiceRepository;
use Ramsey\Uuid\Uuid;
use App\Domain\Enums\StatusEnum;

class InvoiceHelper
{
    private $invoiceRepository;

    public function __construct()
    {
        $this->invoiceRepository = App::make(EloquentInvoiceRepository::class);
    }

    public function getInvoice(): ?Invoice
    {
        $invoiceModel = InvoiceModel::with('company', 'billedCompany', 'products')->first();

        // Check if there is an InvoiceModel in the database
        if (!$invoiceModel) {
            return null; // Return null if no invoice exists
        }

        $company = CompanyModel::find($invoiceModel->company_id);
        $billedCompany = CompanyModel::find($invoiceModel->billed_company_id);

        // Check if there is an company in the database
        if (!$company || !$billedCompany) {
            return null; // Return null if no company exists
        }
        $invoiceModel->company = $company;
        $invoiceModel->billed_company = $billedCompany;

        $invoice = $this->invoiceRepository->toDomain($invoiceModel);

        return $invoice;
    }
        
}