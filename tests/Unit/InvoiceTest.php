<?php

namespace Tests\Unit;

use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Invoices\Api\InvoicesController;
use App\Modules\Invoices\Domain\Mappers\InvoiceMapper;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Helpers\InvoiceHelper;
use App\Modules\Invoices\Infrastructure\Database\Seeders\CompanySeeder;
use App\Modules\Invoices\Infrastructure\Database\Seeders\ProductSeeder;
use App\Modules\Invoices\Infrastructure\Database\Seeders\InvoiceSeeder;
use App\Modules\Invoices\Infrastructure\Database\Seeders\DatabaseSeeder;

class InvoiceTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed(CompanySeeder::class);
        $this->seed(ProductSeeder::class);
        $this->seed(InvoiceSeeder::class);
        $this->seed(DatabaseSeeder::class);
    }

    public function test_invoice_show(): void
    {

        $this->seed(CompanySeeder::class);
        $this->seed(ProductSeeder::class);
        $this->seed(InvoiceSeeder::class);
        $this->seed(DatabaseSeeder::class);
        $repository = $this->app->make(InvoiceRepository::class);
        $approvalFacade = $this->app->make(ApprovalFacadeInterface::class);
        $invoiceMapper = $this->app->make(InvoiceMapper::class);
    
        $controller = new InvoicesController($repository, $approvalFacade, $invoiceMapper);

        $invoiceHelper = new InvoiceHelper();
        $invoice = $invoiceHelper->getInvoice();
        $testInvoiceId = $invoice->id;
        $response = $controller->show($testInvoiceId);

        $this->assertNotNull($response->content());
    }

    public function test_invoice_approve(): void
    {
        $repository = $this->app->make(InvoiceRepository::class);
        $approvalFacade = $this->app->make(ApprovalFacadeInterface::class);
        $invoiceMapper = $this->app->make(InvoiceMapper::class);

        $controller = new InvoicesController($repository, $approvalFacade, $invoiceMapper);

        $invoiceHelper = new InvoiceHelper();
        $invoice = $invoiceHelper->getInvoice();
        $testInvoiceId = $invoice->id;
        
        $response = $controller->approve($testInvoiceId);
        $responseArray = $response->getData(true);
        $message = $responseArray['message'];

        if ($message === 'Invoice approved') {
            $approvedInvoice = $repository->find($testInvoiceId);
            $this->assertEquals('approved', $approvedInvoice->status);
        } elseif ($message === 'approval status is already assigned') {
            $this->assertSame('approval status is already assigned', $message);
        } else {
            $this->fail('Unexpected response message: ' . $message);
        }
    }

    public function test_invoice_reject(): void
    {

        $repository = $this->app->make(InvoiceRepository::class);
        $approvalFacade = $this->app->make(ApprovalFacadeInterface::class);
        $invoiceMapper = $this->app->make(InvoiceMapper::class);
    
        $controller = new InvoicesController($repository, $approvalFacade, $invoiceMapper);

        $invoiceHelper = new InvoiceHelper();
        $invoice = $invoiceHelper->getInvoice();
        $testInvoiceId = $invoice->id;

        $response = $controller->reject($testInvoiceId);
        $responseArray = $response->getData(true);
        $message = $responseArray['message'];

        if ($message === 'Invoice rejected') {
            $approvedInvoice = $repository->find($testInvoiceId);
            $this->assertEquals('rejected', $approvedInvoice->status);
        } elseif ($message === 'approval status is already assigned') {
            $this->assertSame('approval status is already assigned', $message);
        } else {
            $this->fail('Unexpected response message: ' . $message);
        }
    }

}