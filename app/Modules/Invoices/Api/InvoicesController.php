<?php 

namespace App\Modules\Invoices\Api;

use App\Domain\Enums\StatusEnum;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Domain\Invoice;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Invoices\Domain\Mappers\InvoiceMapper;
use App\Modules\Approval\Api\Dto;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\CompanyModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\InvoiceProductLineModel;
use Ramsey\Uuid\UuidInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use Ramsey\Uuid\Uuid;
use LogicException;

class InvoicesController extends Controller
{
    private $invoiceRepository;
    private $approvalFacade;
    private $invoiceMapper;

    public function __construct(InvoiceRepository $invoiceRepository, ApprovalFacadeInterface $approvalFacade, InvoiceMapper $invoiceMapper)
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->approvalFacade = $approvalFacade;
        $this->invoiceMapper = $invoiceMapper;
    }

    public function show(string $id)
    {
        $invoiceDto = $this->invoiceRepository->find($id);

        if (!$invoiceDto) {
            return response()->json(['message' => 'Invoice not found'], 404);
        }

        return response()->json($invoiceDto);
    }

    public function approve(string $id)
    {
        $invoice = $this->invoiceRepository->find($id);

        if (!$invoice) {
            return response()->json(['message' => 'Invoice not found'], 404);
        }

        $uuid = Uuid::fromString($invoice->id);

        if ($uuid instanceof UuidInterface) {

            $approvalDto = new ApprovalDto(
                $uuid,
                StatusEnum::from($invoice->status),
                "App\Modules\Approval\Api\Dto\ApprovalDto\EntityApproved"
            );
        }

        try {
            if ($this->approvalFacade->approve($approvalDto)) {
                $invoice->status = 'approved';
                $this->invoiceRepository->save($invoice);
    
                return response()->json(['message' => 'Invoice approved']);
            }
        } catch (LogicException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }

        return response()->json(['message' => 'Invoice not approvable'], 400);
    }

    public function reject(string $id)
    {
        $invoice = $this->invoiceRepository->find($id);

        if (!$invoice) {
            return response()->json(['message' => 'Invoice not found'], 404);
        }

        $uuid = Uuid::fromString($invoice->id);

        if ($uuid instanceof UuidInterface) {

            $approvalDto = new ApprovalDto(
                $uuid,
                StatusEnum::from($invoice->status),
                "App\Modules\Approval\Api\Dto\ApprovalDto\EntityRejected"
            );
        }

        try {

            $valid = $this->approvalFacade->reject($approvalDto);
            if ($valid) {
                $invoice->status = 'rejected';
                $this->invoiceRepository->save($invoice);

                return response()->json(['message' => 'Invoice rejected']);
            }
        } catch (LogicException $e) {
            return response()->json(['message' => $e->getMessage()], 400);
        }


        return response()->json(['message' => 'Invoice not rejectable'], 400);
    }
}