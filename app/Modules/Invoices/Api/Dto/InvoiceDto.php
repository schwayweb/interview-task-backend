<?php

use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\CompanyModel;
use Ramsey\Uuid\UuidInterface;

final class InvoiceDto
{
    public function __construct(
        public string $id,
        public int $invoice_number,
        public \DateTimeImmutable $invoice_date,
        public \DateTimeImmutable $due_date,
        public CompanyModel $company,
        public CompanyModel $billed_company,
        public array $products,
        public float $total_price,
        public string $status,
    ) {
    }
}