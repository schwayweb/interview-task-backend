<?php

namespace App\Modules\Invoices\Domain;

use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\CompanyModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\ProductModel;
use App\Domain\Enums\StatusEnum;
use Ramsey\Uuid\UuidInterface;

class Invoice
{
   public $id;
   public $invoice_number;
   public $invoice_date;
   public $due_date;
   public $company;
   public $billed_company;
   public $products;
   public $total_price;
   public $status;

   public function __construct(
       string $id,
       string $invoice_number,
       string $invoice_date,
       string $due_date,
       CompanyModel $company,
       CompanyModel $billed_company,
       array $products = [],  
       string $status
   ) {
       $this->id = $id;
       $this->invoice_number = $invoice_number;
       $this->invoice_date = $invoice_date;
       $this->due_date = $due_date;
       $this->company = $company;
       $this->billed_company = $billed_company;
       $this->products = $products;
       $this->total_price = $this->calculateTotalPrice();
       $this->status = $status;
   }

   public function calculateTotalPrice(): float
   {
       $totalPrice = 0;
       foreach ($this->products as $product) {
           $totalPrice += $product["pivot"]["quantity"] * $product["price"];
       }

       return $totalPrice;
   }

   public function approve(): bool
   {
       if ($this->status === StatusEnum::DRAFT) {
           $this->status = StatusEnum::APPROVED;
           return true;
       }

       return false;
   }

   public function reject(): bool
   {
       if ($this->status === StatusEnum::DRAFT) {
           $this->status = StatusEnum::REJECTED;
           return true;
       }

       return false;
   }
}