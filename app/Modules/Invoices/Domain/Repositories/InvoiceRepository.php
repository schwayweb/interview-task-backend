<?php

namespace App\Modules\Invoices\Domain\Repositories;

use App\Modules\Invoices\Domain\Invoice;
use Ramsey\Uuid\UuidInterface;

interface InvoiceRepository
{
    public function find(string $id): ?Invoice;
    public function save(Invoice $invoice): void;
}