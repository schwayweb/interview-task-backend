<?php

namespace App\Modules\Invoices\Domain\Mappers;

use App\Modules\Invoices\Domain\Invoice;
use App\Modules\Invoices\Api\Dto\InvoiceDto;

class InvoiceMapper
{
    public function toDto(Invoice $invoice): InvoiceDto
    {
        return new InvoiceDto(
            $invoice->id,
            $invoice->invoice_number,
            $invoice->invoice_date,
            $invoice->due_date,
            $invoice->company,
            $invoice->billed_company,
            $invoice->products,
            $invoice->total_price,
            $invoice->status
        );
    }
}