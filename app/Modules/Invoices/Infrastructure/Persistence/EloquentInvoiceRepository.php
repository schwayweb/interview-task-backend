<?php

namespace App\Modules\Invoices\Infrastructure\Persistence;

use App\Modules\Invoices\Domain\Invoice;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\InvoiceModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\CompanyModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\ProductModel;
use Ramsey\Uuid\UuidInterface;

class EloquentInvoiceRepository implements InvoiceRepository
{
    public function find(string $id): ?Invoice
    {
        $invoice = InvoiceModel::with('company', 'billedCompany', 'products')->find($id);
        
        if (!$invoice) {
            return null;
        }

        $company = CompanyModel::find($invoice->company_id);
        $billedCompany = CompanyModel::find($invoice->billed_company_id);

        // Check if there is an company in the database
        if (!$company || !$billedCompany) {
            return null; // Return null if no company exists
        }
        $invoice->company = $company;
        $invoice->billed_company = $billedCompany; // fix the typo here

        return $this->toDomain($invoice);
    }

    public function save(Invoice $invoice): void
    {
        // $invoiceModel = new InvoiceModel();
        $invoiceModel = InvoiceModel::find($invoice->id);
        $this->toPersistence($invoice, $invoiceModel);

        $invoiceModel->save();
    }

    public function toDomain(InvoiceModel $invoiceModel): Invoice
    {
        $products = $invoiceModel->products->toArray();
        $invoice = new Invoice(
            $invoiceModel->id,
            $invoiceModel->number,
            $invoiceModel->date,
            $invoiceModel->due_date,
            $invoiceModel->company,
            $invoiceModel->billed_company,
            $products,
            $invoiceModel->status
        );

        return $invoice;
    }

    private function toPersistence(Invoice $invoice, InvoiceModel $invoiceModel): void
    {
        $invoiceModel->status = $invoice->status;
    }
}