<?php

namespace App\Modules\Invoices\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\InvoiceModel;

class CompanyModel extends Model
{
    use HasFactory;
    protected $table = 'companies';
    protected $guarded = [];

    public function invoices()
    {
        return $this->hasMany(InvoiceModel::class, 'company_id');
    }

    public function billedInvoices()
    {
        return $this->hasMany(InvoiceModel::class, 'billed_company_id');
    }
}