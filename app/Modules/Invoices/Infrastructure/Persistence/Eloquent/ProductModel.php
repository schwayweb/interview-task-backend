<?php

namespace App\Modules\Invoices\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
// use Database\Factories\ProductModelFactory;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\InvoiceProductLineModel;

class ProductModel extends Model
{
    use HasFactory;
    protected $table = 'products';
    protected $guarded = [];

    public function invoiceProductLines()
    {
        return $this->hasMany(InvoiceProductLineModel::class, 'product_id', 'id');
    }
}