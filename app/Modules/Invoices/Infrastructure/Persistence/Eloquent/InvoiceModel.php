<?php 

namespace App\Modules\Invoices\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\CompanyModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\ProductModel;
use Ramsey\Uuid\UuidInterface;

class InvoiceModel extends Model
{
    use HasFactory;
    protected $table = 'invoices';
    protected $guarded = [];
    /**
     * Indicate if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The "type" of the auto-generated primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    public function company(): BelongsTo
    {
        return $this->belongsTo(CompanyModel::class, 'company_id', 'id');
    }

    public function billedCompany(): BelongsTo
    {
        return $this->belongsTo(CompanyModel::class, 'billed_company_id', 'id');
    }

    public function products(): BelongsToMany
    {
        return $this->belongsToMany(
            ProductModel::class, 
            'invoice_product_lines', 
            'invoice_id',
            'product_id'
        )->withPivot('quantity');
    }
}