<?php

namespace App\Modules\Invoices\Infrastructure\Persistence\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\InvoiceModel;
use App\Modules\Invoices\Infrastructure\Persistence\Eloquent\ProductModel;

class InvoiceProductLineModel extends Model
{
    use HasFactory;

    protected $table = 'invoice_product_lines';
    protected $guarded = [];

    public function product(): BelongsTo
    {
        return $this->belongsTo(ProductModel::class, 'product_id', 'id');
    }

    public function invoice(): BelongsTo
    {
        return $this->belongsTo(InvoiceModel::class, 'invoice_id', 'id');
    }
}