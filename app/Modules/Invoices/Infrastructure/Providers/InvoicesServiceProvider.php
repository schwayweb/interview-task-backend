<?php 

namespace App\Modules\Invoices\Infrastructure\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use App\Modules\Invoices\Domain\Repositories\InvoiceRepository;
use App\Modules\Invoices\Infrastructure\Persistence\EloquentInvoiceRepository;

class InvoicesServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerRoutes();
    }

    public function register()
    {
        $this->app->bind(InvoiceRepository::class, EloquentInvoiceRepository::class);
    }

    protected function registerRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace('App\Modules\Invoices\Api')
            ->group(function () {
                Route::get('/invoices/{id}', 'InvoicesController@show');
                Route::post('/invoices/{id}/approve', 'InvoicesController@approve');
                Route::post('/invoices/{id}/reject', 'InvoicesController@reject');
            });
    }
}